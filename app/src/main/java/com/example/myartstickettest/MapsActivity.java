package com.example.myartstickettest;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;
import android.os.AsyncTask;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.ViewGroup;
import android.view.View;
import android.content.Intent;
import android.net.Uri;
import org.json.JSONArray;
import org.json.JSONObject;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        new TransTask()
                .execute("http://api.winjet.com.tw/api/web/GetPerformanceJsonByLocation.php");
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(25.0515321, 121.5537864);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 17));
    }

    class TransTask extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... params) {
            StringBuilder sb = new StringBuilder();
            try {
                URL url = new URL(params[0]);
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(url.openStream()));
                String line = in.readLine();
                while(line!=null){
                    Log.d("HTTP", line);
                    sb.append(line);
                    line = in.readLine();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return sb.toString();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("JSON", s);
            parseJSON(s);
        }
    }


    private void parseJSON(String s) {

        LinearLayout linearLayout1 = findViewById(R.id.line_id);

        TextView firstText = new TextView(this);
        firstText.setId(R.id.debug);
        LinearLayout.LayoutParams layoutParamsFirst = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        firstText.setText("成功！");
        firstText.setLayoutParams(layoutParamsFirst);
        linearLayout1.addView(firstText);


        //加入一個左右的區塊
        LinearLayout linearLayoutDemo = new LinearLayout(this);
        LinearLayout.LayoutParams layoutParamsDemo = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        linearLayoutDemo.setLayoutParams(layoutParamsDemo);
        linearLayoutDemo.setOrientation(LinearLayout.HORIZONTAL);
        linearLayoutDemo.setWeightSum(7);
        linearLayout1.addView(linearLayoutDemo);

        //左側圖片
        LinearLayout linearLayoutDemoRight = new LinearLayout(this);
        LinearLayout.LayoutParams paramRight = new LinearLayout.LayoutParams(
                0,
                ViewGroup.LayoutParams.MATCH_PARENT,
                2.0f
        );
        linearLayoutDemoRight.setLayoutParams(paramRight);
        linearLayoutDemoRight.setOrientation(LinearLayout.VERTICAL);
        linearLayoutDemo.addView(linearLayoutDemoRight);

        //取得線上的圖片
        ImageView imageViewDemo = new ImageView(this);
        LinearLayout.LayoutParams paramImage = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );

        //paramImage.height = 400;
        imageViewDemo.setLayoutParams(paramImage);

        Glide.with(this).load("https://static.utiki.com.tw/Data/KHAM/Images/UTK2401/N02OB78V.JPG").into(imageViewDemo);
        linearLayoutDemoRight.addView(imageViewDemo);

        //右側節目說明
        LinearLayout linearLayoutDemoLeft = new LinearLayout(this);
        LinearLayout.LayoutParams paramLeft = new LinearLayout.LayoutParams(
                0,
                ViewGroup.LayoutParams.MATCH_PARENT,
                5.0f
        );
        linearLayoutDemoLeft.setLayoutParams(paramLeft);
        linearLayoutDemoLeft.setOrientation(LinearLayout.VERTICAL);
        linearLayoutDemo.addView(linearLayoutDemoLeft);

        //放入演出場地及演出名稱
        TextView textViewPerformanceName = new TextView(this);
        LinearLayout.LayoutParams layoutParamsPerformanceName = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textViewPerformanceName.setLayoutParams(layoutParamsPerformanceName);
        textViewPerformanceName.setText("演出的名稱");
        linearLayoutDemoLeft.addView(textViewPerformanceName);

        TextView textViewVenueName = new TextView(this);
        LinearLayout.LayoutParams layoutParamsVenueName = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textViewVenueName.setLayoutParams(layoutParamsVenueName);
        textViewVenueName.setText("場地的名稱");
        linearLayoutDemoLeft.addView(textViewVenueName);

        //加入馬上購買的button
        Button buttonBuy = new Button(this);
        buttonBuy.setText(R.string.buy_now);
        buttonBuy.setTag("https://www.taaze.tw");
        buttonBuy.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                String goUrl = v.getTag().toString();
                Intent browserIntent = new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(goUrl));
                startActivity(browserIntent);

            }
        });
        linearLayoutDemoLeft.addView(buttonBuy);


        try
        {
            JSONArray jsonPerformances = new JSONArray(s);

            for(int i = 0; i < jsonPerformances.length() ; i++){

                JSONObject performance = jsonPerformances.getJSONObject(i);

                //int eachId = performance.getInt("id");
                String performanceName = performance.getString("name");
                String startDate = performance.getString("start_datetime");
                String endDate = performance.getString("end_datetime");
                //String imageUrl = performance.getString("image_url");
                JSONObject venue = performance.getJSONObject("venue");
                String venueName = venue.getString("name");
                //double northDegree = venue.getDouble("north_degree");
                //double eastDegree = venue.getDouble("east_degree");
                //String address = venue.getString("address");

                /*
                TextView textView = new TextView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                String mString = String.format( "活動名稱: %s 活動地點： %s 活動起迄時間： %s ~ %s ",  performanceName, venueName, startDate, endDate);
                textView.setText(mString);
                textView.setLayoutParams(layoutParams);
                linearLayout1.addView(textView);
                */
            }


        }
        catch(Exception ex)
        {
            //把錯誤接在一個文字中
            firstText.setText(ex.toString());
        }


    }
}
